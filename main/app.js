function distance(first, second){


	//verificare daca este array
	if(!Array.isArray(first))throw new Error('InvalidType');
	if(!Array.isArray(second))throw new Error('InvalidType');

	//compararea cu vide
	if(first.length===0 ||first===undefined){
		console.log('First array is  empty')
		return 0;
	}else {
		console.log('First array is not empty')
	}
	if(second.length===0||second===undefined){
		console.log('Second array is  empty')
		return 0
	}else{
		console.log('Second array is not empty')
	}
	//eliminare duplicate
	var firstUniques = Array.from(new Set(first))
	var secondUniques = Array.from(new Set(second))
	console.log(firstUniques)
	console.log(secondUniques)

	//diferenta dintre cele 2 array-uri (elementele necomune)
	let difference = firstUniques
                 .filter(x => !secondUniques.includes(x))
                 .concat(secondUniques.filter(x => !firstUniques.includes(x)))

				 console.log(difference)
				 console.log(difference.length)

	return difference.length
	//TODO: implementați funcția
	// TODO: implement the function
}

distance(['a','b'], ['a'])

module.exports.distance = distance